class AdminsController < ApplicationController
	def index
		@posts = Post.all.order("created_at DESC")
		render 'posts/userIndex'
	end
	def listUsers
		@users = User.all.order("id")
	end
	def listAllLogs
		if @logs = Activitylog.all.order("created_at DESC")
			render "listLogs"
		else
			render :json 
		end
	end
	def listLogs
		@logs = Activitylog.where(user_id: params[:id]).order("created_at DESC")
	end
end
