class ApplicationController < ActionController::Base
	before_filter :configure_permitted_parameters, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.record_activity("Created new account")
  # For APIs, you may want to use :null_session instead.
  	protect_from_forgery with: :exception

  	layout 'application'
    before_filter :set_constants

    def set_constants
    	if user_signed_in?
    		@lists = Post.where(user_id: current_user.id).order("created_at DESC")
    	end
    end

  	def record_activity(note)
	    @activity = Activitylog.new
	    if user_signed_in?
	   		@activity.user_id = current_user.id
	    else
	    	@activity.user_id = "guest"
	    end
	    @activity.note = note
	    @activity.browser = request.env['HTTP_USER_AGENT']
	    @activity.ip_address = request.env['REMOTE_ADDR']
	    @activity.controller = controller_name 
	    @activity.action = action_name 
	    @activity.params = params.inspect
	    # @activity.save
	end

  	protected

	  def configure_permitted_parameters
	    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation) }
	  end
end
