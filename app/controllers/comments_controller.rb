class CommentsController < ApplicationController
	before_action :authenticate_user!
	def create
		@post = Post.find(params[:post_id])
		@comment = Comment.new 
		@comment.user_id = current_user.id 
		@comment.post_id = params[:post_id] 
		@comment.body = params[:comment][:body]
		@comment.save
		# @comment = @post.comments.create(params[:comment].permit(:body) )
		redirect_to post_path(@post)		
	end
	def destroy
	    @post = Post.find(params[:post_id])
	    @comment = @post.comments.find(params[:id])
	    if @comment.user_id == current_user.id || @comment.post.user == current_user || current_user.admin
		    @comment.destroy
		end
		redirect_to post_path(@post)
	end
end
