require 'json'

class PostsController < ApplicationController

	before_action :authenticate_user!, :only => [:new, :edit, :update, :create, :destroy]

	def new	
		@post = Post.new
	end

	def edit
		@post = Post.find(params[:id])		
	end

	def update
	  	@post = Post.find(params[:id])
	  	if @post.user_id == current_user.id || current_user.admin
		  if @post.update(params[:post].permit(:title, :text))
		    redirect_to @post
		  else
		    render 'edit'
		  end
		else
			redirect_to user_index_path(current_user.id)
		end
	end

	def index
	end

	def userIndex
		if current_user.admin && params[:id].to_i == current_user.id.to_i	# redirect for admin to admins controller
			redirect_to admin_index_path
		elsif current_user.id.to_i == params[:id].to_i # seeing own feed
			@posts = Post.where("created_at > ?", Time.now - 1.month).order("created_at DESC").limit(10)
			record_activity("Visited own feed of user.id=" + params[:id].to_s)
		else											# seeing someone's profile/index
			@posts = Post.where(user_id: params[:id])
			record_activity("Visited profile of user.id=" + params[:id].to_s)
		end
	end

	def tags
		if params[:tag]
			@posts = Post.tagged_with(params[:tag])
			puts @posts
			render "userIndex"
		end
	end

	def tags_list	
	end

	def create
		@post = Post.new post_params
		@post.user_id = current_user.id
		if @post.save
			respond_to do |format|
				record_activity("Created new account")
				format.html { redirect_to post_path(@post), notice: 'Post was successfully created.' }
	        	format.json { render json: post_path(@post), status: :created, location: @post }	 
	        end
		else
			respond_to do |format|
				format.html { render action: "new" }
		        format.json { render json: user_index_path(current_user.id).errors, status: :unprocessable_entity }
		    end
		end
	end

	def show
		record_activity("Visited post.id=" + params[:id].to_s)
		@post = Post.find(params[:id])
	end

	def destroy
	  @post = Post.find(params[:id])
	  if @post.user_id == current_user.id || current_user.admin
	 	@post.destroy
	 	record_activity("Deleted post.id=" + params[:id].to_s)
	  end
	  redirect_to user_index_path(current_user.id)
	end

	def search
		@posts = Post.search(params[:search])
		render 'userIndex'
	end

	private

		def post_params
			params.require(:post).permit(:title, :text, :tag_list)
		end
end
