class Post < ActiveRecord::Base
	belongs_to :user
	has_many :comments, dependent: :destroy	
	validates :title, presence: true, length: { minimum: 5 }
	acts_as_taggable
	acts_as_taggable_on :tags


	def self.search(search)
	  if search
	    find(:all, :conditions => ['text or title LIKE ?', "%#{search}%"])
	  else
	    find(:all)
	  end
	end


end
