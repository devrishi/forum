class SetPostIdNotNullInComments < ActiveRecord::Migration
  def change
  	change_column :comments, :post_id, :integer, :null => false
  end
end
