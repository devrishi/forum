class CreateActivitylogs < ActiveRecord::Migration
  def change
    create_table :activitylogs do |t|
      t.string :user_id, :null => false
	  t.string :browser
	  t.string :ip_address
	  t.string :controller
	  t.string :action
	  t.string :params
	  t.string :note
	  t.datetime :created_at
	  t.datetime :updated_at	

      t.timestamps
    end
  end
end
